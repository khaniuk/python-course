from tkinter import Tk, Label, Entry, Button, messagebox, StringVar


class app():
    # define screen size
    custom_width = 500
    custom_height = 320

    def __init__(self, root):
        self.window = root
        self.window.title('Form')
        self.inputValue = StringVar()

        # get width and height of screen for position
        x = (self.window.winfo_screenwidth()//2) - (self.custom_width//2)
        y = (self.window.winfo_screenheight()//2) - (self.custom_height//2)
        # self.window.geometry(str(x)+'x'+str(y))
        self.window.geometry(str(self.custom_width)+'x' +
                             str(self.custom_height)+"+"+str(x)+"+"+str(y))
        # block resize
        self.window.resizable(width=False, height=False)

        # Label
        Label(self.window, text="Type a text").pack()
        # Entry
        Entry(self.window, textvariable=self.inputValue).pack()
        # Label
        self.txt = Label(self.window, text="Result typed text")
        self.txt.config(fg="#009688")
        self.txt.pack()
        # Button
        Button(self.window, text="Click to Get Value",
               command=self.getEntryValue).pack()

    def getEntryValue(self):
        if self.inputValue.get():
            self.txt["text"] = "Result typed text: "+self.inputValue.get()
            # lbl.config(text=inputValue.get())
        else:
            messagebox.showinfo("Alert", "Type in box")


if __name__ == "__main__":
    root = Tk()
    app(root)
    root.mainloop()
