from tkinter import Tk, Label, Entry, Button, messagebox, StringVar


def getEntryValue():
    if inputValue.get():
        txt["text"] = "Result typed text: "+inputValue.get()
        # lbl.config(text=inputValue.get())
    else:
        messagebox.showinfo("Alert", "Type in box")
        # print(inputValue.get())


window = Tk()
window.title('Form')

inputValue = StringVar()
# define screen size
custom_width = 500
custom_height = 320

# get width and height of screen for position
x = (window.winfo_screenwidth()//2) - (custom_width//2)
y = (window.winfo_screenheight()//2) - (custom_height//2)

# window.geometry(str(x)+'x'+str(y))
window.geometry(str(custom_width)+'x'+str(custom_height)+"+"+str(x)+"+"+str(y))

# block resize
window.resizable(width=False, height=False)

# Label
lbl = Label(window, text="Type a text")
lbl.pack()
# Entry
Entry(window, textvariable=inputValue).pack()
# Label
txt = Label(window, text="Result typed text")
txt.config(fg="#009688")
txt.pack()
# Button
Button(window, text="Click to Get Value", command=getEntryValue).pack()

window.mainloop()
