from tkinter import Tk, Button, messagebox, colorchooser


def getColor():
    color = colorchooser.askcolor(title="Select a color")
    print(color)
    # print(color[1])  # get only hexa


text = "Python - Module III"
window = Tk()
window.title('Pick colors')
window.geometry('500x400')
# Button
Button(window, text="Color Picker", command=getColor).pack()

window.mainloop()
