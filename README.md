# Python Course - Module III

## Python Avanzado

Contenido del curso

| Nro. | Descripción                                              |
| :--: | -------------------------------------------------------- |
| 1 .  | Interfaces graficas en Python                            |
| 2 .  | Manejo de wxPython para la interfaz gráfica              |
| 3 .  | Creación de ventanas para una aplicación en Python       |
| 4 .  | Manejo de menús (interactivos)                           |
| 5 .  | Conectividad con una base de datos (MySql y SqLite)      |
| 6 .  | Leer datos desde una interfaz gráfica                    |
| 7 .  | Desarrollo de un proyecto de escritorio (Python y MySql) |
| 8 .  | Leer archivos web utilizando Python                      |
| 9 .  | BeautifulSoup en Python                                  |
