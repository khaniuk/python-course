from tkinter import Tk, Button, filedialog


def openFile():
    filepath = filedialog.askopenfilename(title="Select a file")
    print(filepath)


def createFile():
    myfile = filedialog.asksaveasfile(
        title="Save file", mode="w", defaultextension=".txt")
    if myfile is not None:
        myfile.write("Lorem ipsum text")
        myfile.close()


text = "Python - Module 3"
window = Tk()
window.title('File')
window.geometry('500x400')
# Button
Button(window, text="Open file", command=openFile).pack()
Button(window, text="Create file", command=createFile).pack()
# Button(window, text="Show Error", command=showError).pack()
# Button(window, text="Show Question", command=showQuestion).pack()

window.mainloop()
