from tkinter import Tk, Button, messagebox


def showInfo():
    messagebox.showinfo("Info", "Hello info message")


def showWarning():
    messagebox.showwarning("Warning", "Hello warming message")


def showError():
    messagebox.showerror("Error", "Hello error message")


def showQuestion():
    messagebox.askquestion("Ask question", "Hello ask question")


window = Tk()
window.title('Alerts')
window.geometry('500x400')
# Button
Button(window, text="Show Info", command=showInfo).pack()
Button(window, text="Show Warning", command=showWarning).pack()
Button(window, text="Show Error", command=showError).pack()
Button(window, text="Show Question", command=showQuestion).pack()

window.mainloop()
